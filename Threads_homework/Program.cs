﻿using System.Diagnostics;

Stopwatch sw = new Stopwatch();

long[] array1 = new long[100000];
long[] array2 = new long[1_000_000];
long[] array3 = new long[10_000_000];

long sum1 = 0;
long sum2 = 0;
long sum3 = 0;

Random random = new Random();

for(int i = 0; i < 100000; i++)
{
    array1[i] += i;
}

for (int i = 0; i < 1_000_000; i++)
{
    array2[i] += i;
}

for (int i = 0; i < 10_000_000; i++)
{
    array3[i] += i;
}

sw.Start();
foreach (int item in array1)
{
    sum1 += item;
}
sw.Stop();
Console.WriteLine($"Result foreach for 100000: {sw.ElapsedMilliseconds}");

sw.Restart();
foreach (int item in array2)
{
    sum2 += item;
}
sw.Stop();
Console.WriteLine($"Result foreach for 1000000: {sw.ElapsedMilliseconds}");

sw.Restart();
foreach (int item in array3)
{
    sum3 += item;
}
sw.Stop();
Console.WriteLine($"Result foreach for 10000000: {sw.ElapsedMilliseconds}");

sum1 = 0;
sw.Restart();
sum1 = array1.AsParallel().Sum();
sw.Stop();
Console.WriteLine($"Result LINQ for 100000: {sw.ElapsedMilliseconds}");

sum2 = 0;
sw.Restart();
sum2 = array2.AsParallel().Sum();
sw.Stop();
Console.WriteLine($"Result LINQ for 1000000: {sw.ElapsedMilliseconds}");

sum3 = 0;
sw.Restart();
sum3 = array3.AsParallel().Sum();
sw.Stop();
Console.WriteLine($"Result LINQ for 10000000: {sw.ElapsedMilliseconds}");

sw.Restart();
ThreadSum(array1);
sw.Stop();
Console.WriteLine($"Result Threads for 100000: {sw.ElapsedMilliseconds}");

sw.Restart();
ThreadSum(array2);
sw.Stop();
Console.WriteLine($"Result Threads for 100000: {sw.ElapsedMilliseconds}");

sw.Restart();
ThreadSum(array3);
sw.Stop();
Console.WriteLine($"Result Threads for 100000: {sw.ElapsedMilliseconds}");

long ThreadSum(long[] array)
{
    long totalSum = 0;
    long[] sum = new long[10];
    Thread[] threads = new Thread[10];
    for(int i = 0; i < 10; i++)
    {
        int tmp = i;
        threads[i] = new Thread(() => SumMethod(tmp, array, sum));
        threads[i].Start();
    }
    for (int i = 0; i < 10; i++)
    {
        threads[i].Join();
    }
    for(int i = 0; i < 10; i++)
    {
        totalSum += sum[i];
    }
    return totalSum;
}

void SumMethod(int threadId, long[] array, long[] sum)
{
    int id = threadId;
    for(int i = id *(array.Length / 10); i< (id+1) * (array.Length) / 10; i++)
    {
        sum[id] += array[i];
    }
}


